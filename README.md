# Genki 1: Pronunciation
Documentation of the Pronunciation of the first few Lessons in Genki 1
# Also contains
## A collection of Anki Decks for Lessons 1-20
#### These Decks include
- Vocabulary
- Grammar excercises
- Kanji Pronunciation excercises

## A list of the Kanji as they occur in every Lesson
