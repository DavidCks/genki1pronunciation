# Genki 1: Vocabulary
When/If it is completed, this should be a nice list of Decks to use for the Anki App

Important addition to the Kanji Deck for Genki 1 Lesson 3
This Deck is a one of a kind Deck for you to experiment with.
If you think that learning your Kanji that way is good enough, then you'll have to continue the Collection yourself.
However I'd recommend you use something like Kanji study for practicing Kanji. The full version costs 14€.
I'll put a list of Kanji for every Lesson in the sectoin below, so you'll just have to copy and paste it into the search bar.

## List of Kanji in Genki 1

#### Lesson 3: Kanji from Vocabulary
映画音楽雑誌朝飯酒晩昼水家学校明日今週末土曜毎行帰聞飲話読起食寝見来勉強早全然時々
#### Lesson 3: Kanji from Study Guide
一二三四五六七八九十百千万円時
#### Lesson 4: Kanji from Study Guide
日本人月火水木金王曜上下中半森園林酒竹公
#### Lesson 5: Kanji from Study Guide
山私元天今見行田飲女気男川食岩畑石泳永飛機
#### Lesson 6: Kanji from Study Guide
降寝座返遊東西南北口出右左分先生大学外国
#### Lesson 7: Kanji from Study Guide
京小子会社母父高校毎語文帰入低太速短頭晩
#### Lesson 8: Kanji from Study Guide
員新聞作仕事電車休言読思次何貝耳門取洗
#### Lesson 9: Kanji from Study Guide
泊雲故覚薬午後前名白雨書友間家話少古知来